'use strict';

export class Map{
    constructor(size){
        this.size = Math.pow(2, size) + 1;
        this.mesh = this.generateMesh();
    }

    generateMesh(){
        let map = [];
        for (let i = 0; i < this.size; i++) {
            map.push([]);
            for (let y = 0; y < this.size; y++) {
                map[i].push(0);
            }
        }

        let x = this.size - 1;
        console.log(x);
        
        map[0][0] =  Math.floor((Math.random() * 10));
        map[x][0] =  Math.floor((Math.random() * 10));
        map[0][x] =  Math.floor((Math.random() * 10));
        map[x][x] =  Math.floor((Math.random() * 10));

        let z = Math.ceil(x/2);
        map[z][z] = Math.ceil((map[0][0] + map[x][0] + map[0][x] +  map[x][x])/4);

        x = this.divideStep(x);
        console.log(x);
        return map;
    }

    divideStep(i){
        return i/2 - 1;
    }

    diamondSquare(){
        let h = this.size; // 2^3 + 1 = 9
        let i = this.size - 1; // 8
        while(i > 1) {
            let step = i/2; // 4 // 2 // 1 // 0.5
            for (let x = step; x < h; x + i) { // x = 4; 4 < 9; x + 8 // x = 2; 2 < 9; x + 4 // x = 1; 1 < 9; x + 2 // x = 0.5; 0.5 < 9; x + 1

            }
            i = step; // 4 // 2 // 1
        }
    }

    square(){

    }

    averageNeighboursSquare(x, y){

    }

    printToConsole(){
        for (let i = 0; i < this.mesh.length; i++) {
            console.log(this.mesh[i].join(''));
        }
    }
}