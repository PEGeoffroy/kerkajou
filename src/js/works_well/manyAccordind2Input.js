"use strict";

// Done
// The purpose is to inform in the console many differents pokemon names when clicking according to input

let number = 1;
let url = `https://pokeapi.co/api/v2/pokemon/`;
let manyAccordind2Input = document.getElementById('manyAccordind2Input');

function inputValue() {
    return parseInt(document.getElementById('inputValue').value);   
}

async function asyncCall() {
    let result = Array.from(Array(inputValue()), (x, i) => i).map(i => i + number);

    const data = Promise.all(
        result.map(async (i) => await (await fetch(`${url}${i}`)).json())
    )
    return data
}

manyAccordind2Input.addEventListener("click", function () {
    asyncCall()
        .then(data => {
            data.forEach(pokemon => {
                console.info(`${pokemon.name}`)
            });
        })
    number += inputValue();
}, false);