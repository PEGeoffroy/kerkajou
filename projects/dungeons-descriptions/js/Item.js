"use strict";

let main = document.getElementById('main');
let condition = true;

main.addEventListener('click', (event) => {
  if (condition === true) {
    let inputValue = document.getElementById('input').value;
    let item = new Item(event.clientX, event.clientY, inputValue);
    item.displayItem();  }
}, false);

export default class Item {
  constructor(x, y, input = '', mainColor = '#795548', mateColor = 'black') {
    this.mainColor = mainColor;
    this.mateColor = mateColor;
    this.text = input;
    this.location = { "x": x, "y": y };
    this.size = 16;
  }

  displayItem() {
    let item = document.createElement('div');
    item.classList.add('item');
    let note = document.createElement('div');
    note.classList.add('note');
    let text = document.createElement('p');
    text.classList.add('text');
    let deleteButton = document.createElement('button');
    deleteButton.classList.add('deleteButton');

    deleteButton.textContent = '-';
    text.textContent = `${this.text}`;

    note.style.top = `0px`;
    note.style.left = `0px`;

    item.style.width = `${this.size}px`;
    item.style.height = `${this.size}px`;
    item.style.top = `${this.location.y - (this.size / 2)}px`;
    item.style.left = `${this.location.x - (this.size / 2)}px`;

    item.addEventListener('mouseover', () => {
      item.style.backgroundColor = this.mainColor;
      condition = false;
      console.log(condition);
      note.style.display = 'flex';
    }, false);

    item.addEventListener('mouseout', () => {
      item.style.backgroundColor = this.mateColor;
      condition = true;
      console.log(condition);
      note.style.display = 'none';
    }, false);

    deleteButton.addEventListener('click', () => {
      item.remove();
    }, false);

    note.appendChild(deleteButton);
    note.appendChild(text);
    item.appendChild(note);
    main.appendChild(item);
  }

  moveItem() {

  }

  editItem() {

  }

  deleteItem() {

  }
}
