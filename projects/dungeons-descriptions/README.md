# DungeonsDescriptions

## Resume

Récupérer les cartes -> [one-page-dungeon](https://watabou.itch.io/one-page-dungeon)
Les importer via l'ordinateur
Faire un click add description, création d'un node, et l'ajout d'un hover avec les infos dessus

Faire une description générale des lieux genre type pierre, ambiance, les lieux, l'odeur, la texture de la roche description colonne type fuite, type de dégât sur l'environnement.

Spécifier les caractéristiques de salles pour les reconnaître, leurs fonctions.

## Todo

* [ ] Faire un export json avec les nodes et l'image
* [ ] Ajouter l'attribution
* [ ] Ajouter une couleur de node, changer le style
* [ ] Ajouter un switch pour tout afficher
* [ ] Ajouter un toggle pour bloquer/activer l'ajout de notes, et faire un raccourcis
* [ ] Ajouter l'édition de la note
* [ ] Ajouter l'image
* [ ] Faire der filtres (ennemi, description, objet) pour les faire apparaître disparaître, faire des puces à plusieurs type.
* [ ] Génération partielle des descriptions

## Useful links

* [MouseEvent clientX Property](https://www.w3schools.com/jsref/event_clientx.asp)
* [Mouseover](https://developer.mozilla.org/fr/docs/Web/API/Element/mouseover_event)
* [fancy-border-radius](https://9elements.github.io/fancy-border-radius/#50.100.0.50--.)

* [Upload d'image](https://blog.lesieur.name/coder-proprement-en-javascript-par-l-exemple-upload-d-image/), [Codepen](https://codepen.io/Haeresis/pen/aZvQVQ)
* [async await in image loading](https://stackoverflow.com/questions/46399223/async-await-in-image-loading)
* [MDN : async function](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/async_function)
* [How To Display Uploaded Image In Html Using Javascript ?](https://www.webtrickshome.com/faq/how-to-display-uploaded-image-in-html-using-javascript)
