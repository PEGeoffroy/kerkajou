"use strict";

// Done
// The button 'JSON-SEVER' has to inform in the console the content of JSON-SERVER

let url = `http://localhost:3005/creatures`;
let button = document.getElementById('button');

async function asyncCall(url) {
    let response = await fetch(url);
    let data = await response.json();

    return data;
};

button.addEventListener("click", function () {
    asyncCall(`${url}`)
        .then((data) => {
            for (let [key, value] of Object.entries(data)) {
                console.info(`${key} - ${value["name"]} - ${value["strength"]}`);   
            }
        });
}, false);