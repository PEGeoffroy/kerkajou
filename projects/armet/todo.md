# Todo

* [x] Compléter les cartes
* [ ] Faire un système de langues
* [ ] Générer un export en image mentale de cette situation pour la mémoriser pour plus tard
* [ ] Générer une clef en image mentale une suite de chiffre pour s'entraîner à la mémorisation
* [ ] Faire l'alphabet
* [ ] Rajouter une propriété pour l'apprentissage, le nombre d'échec
* [ ] Faire une sauvegarde dans le cache
* [ ] Quel paquet veux tu réviser ?
* [ ] Combien de carte veux tu réviser ?
* [ ] Faire une séléction aléatoire entre le recto et le verso, et comptabiliser les points en fonction
* [ ] Placement bouton -> droitier/gaucher
* [ ] Put \<p> into section
* [ ] Faire un accès à la visualisation des cartes (10 par 10 ?)
* [ ] Projet tdr non-violente, accessible, inclusive
* [ ] Faire des catégories
* [ ] Ajouter le texte et l'id dans la conditionnelle pour alleger le code et rendre les elements réutilisable
* [ ] Au lieu du texte mettre des logos (Vrai, Faux, Commencer, Réponse) -> [Icons](https://www.flaticon.com/packs/interface-icon-assets)
* [ ] Ajouter d'autres cartes
* [ ] Ajouter les variables css
* [ ] Changer la méthode de création des cartes pour qu'elle puisse prendre des index en alphabet.
* [ ] Faire un avant propos sur la table rappel et une autre page qui explique la conception de celle-ci. Accessible via un bouton information
* [ ] Finaliser le changement des datas
* [ ] Ajouter hover pour le style
* [ ] Rajouter un bouton reload pour relancer une session similaire

## Loader : code sous le coude, mais probablement inutile

```index.html
<div id="loader"></div>
```

```main.js
let loader = document.getElementById('loader');
loader.style.backgroundColor = '#6699ff';
setTimeout(() => loader.remove(), 2000);
```

```main.css
#loader{
    width: 25px;
    height: 25px;
    border-radius: 90px;
    background-color: black;
}
```

## Notes

* [Material color panel](https://material.io/resources/color/#!/?view.left=0&view.right=0&primary.color=795548&secondary.color=795548&primary.text.color=212121&secondary.text.color=212121)
* [Material card Example](https://material.io/components/cards/#)

![panel1](img/panel1.png)
![panel2](img/panel2.png)
