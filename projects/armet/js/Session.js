"use strict";

export default class Session {
  constructor(cards, cardsNumber) {
    this.cards = cards;
    this.duration = 0; // faire en fonction du temps ?
    this.cardsNumber = cardsNumber;
    this.cardsToReview = [];
    this.cardsLearned = [];
    this.cardsForgotten = [];
    this.sessionPosition = 0;
  }

  createSession() {
    for (let i = 0; i < this.cardsNumber; i++) {
      console.log(`--> ${i}`);
    }
  }
}