"use strict";

let body = document.body;
let main = document.createElement('main');
main.id = 'card';
let title = document.createElement('p');
title.id = 'title';
let answerSection = document.createElement('section');
answerSection.id = 'answerSection';
let questionSection = document.createElement('section');
questionSection.id = 'questionSection';
let answerButton = document.createElement('p');
answerButton.textContent = 'Réponse';
answerButton.id = 'answerButton';
let answerGood = document.createElement('p');
answerGood.textContent = 'Juste';
answerGood.id = 'answerGood';
let answerBad = document.createElement('p');
answerBad.textContent = 'Faux';
answerBad.id = 'answerBad';

// answerButton.classList.add("button");
// answerGood.classList.add("button");
// answerBad.classList.add("button");

export default class Deck {
    constructor(cards) {
        this.cards = cards;
        this.length = cards.length;
        this.score = { "total": 0, "goodAnswer": 0 };
        this.cardsSelection = [];
    }

    displayCard() {
        main.innerHTML = '';
        let card = this.drawRandomCard();
        title.textContent = card.recto;

        body.appendChild(main);
        questionSection.appendChild(title);
        answerSection.appendChild(answerButton);
        main.appendChild(questionSection);
        main.appendChild(answerSection);

        answerSection.addEventListener('click', function () {
            title.textContent = card.verso;
            answerSection.innerHTML = '';
            answerSection.appendChild(answerGood);
            answerSection.appendChild(answerBad);
        }, false);
    }

    drawRandomCard() {
        return this.cards[Math.ceil((Math.random() * this.length) - 1)];
    }

    drawManyRandomCards(number) {
        if (number > this.length) number = this.length; // Safety to avoid exceeding the maximum
        this.cardsSelection = [];
        let randomArray = [];
        for (let i = 0; i < number; i++) {
            let random = Math.ceil((Math.random() * this.length) - 1);
            if (randomArray.includes(random)) {
                i--;
            } else {
                randomArray.push(random);
                let card = this.cards[random];
                this.cardsSelection.push(card);
            }
        }
        return this.cardsSelection;
    }
}
