"use strict";

export default class Card {
    constructor(recto, verso, trick) {
        this.recto = recto;
        this.verso = verso;
        this.trick = trick;
        this.score = 0;
    }
}

        // console.log(`Les cartes sont des instances de *Card* : ${dataArray[1] instanceof Card}`);
