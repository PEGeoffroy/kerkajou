"use strict";

// This class must be the only one with display methods

import Card from './Card.js';
import Deck from './Deck.js';
import Session from './Session.js';

let header = document.getElementById('header');
let main = document.getElementById('main');
let nav = document.getElementById('nav');

let title = document.createElement('h1');
title.textContent = 'Armet';
let input = document.createElement('input');

export default class Navigation {
    constructor() {
        // You can add several tables here, the name must be the same as the file name (json).
        this.allTables = ['reminder', 'alphabet'];
        this.dataArray = [];
        this.state = '';
        this.revisionsNumber = 10;
    }

    displayNavigation() {
        this.cleanWindow();
        input.value = this.revisionsNumber;
        nav.appendChild(input);
        main.appendChild(title);
        main.style.backgroundColor = '#F5F5F6';

        this.allTables.forEach(tableName => {
            let button = document.createElement('section');
            let text = document.createElement('p');
            text.textContent = tableName;
            button.appendChild(text);
            nav.appendChild(button);

            button.addEventListener('click', () => {
                this.revisionsNumber = input.value; // intérêt limité de cette ligne
                this.displayDeck(tableName, this.revisionsNumber);
            }, false);
        });
    }

    displayDeck(deck, number) {
        this.deckCall(deck)
            .then((data) => {
                let i = 0;
                for (let [key, value] of Object.entries(data)) {
                    let newCard = new Card(key, value.recto, value.trick);
                    this.dataArray[i] = newCard;
                    i++;
                }
                let deck = new Deck(this.dataArray);
                deck.displayCard();
                console.log(deck.drawManyRandomCards(number));
            });
    }

    cleanWindow() {
        header.innerHTML = '';
        main.innerHTML = '';
        nav.innerHTML = '';
    }

    createNewSession(){
        
    }

    async deckCall(deck) {
        let response = await fetch(`./data/${deck}.json`);
        let data = await response.json();
        return data;
    };
}



