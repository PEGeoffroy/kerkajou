import htmlToImage from 'html-to-image';

let button = document.getElementById('button');
let main = document.getElementById('main');

button.addEventListener("click", () => {
    htmlToImage.toPng(document.getElementById(main))
    .then(function (dataUrl) {
        download(dataUrl, 'card.png');
    });
}, false);

