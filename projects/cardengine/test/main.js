let i = 0;

fetch('https://fr.fantasynamegenerators.com/dnd-noms-elfes.php', {method: 'GET', timeout: 5000, keepalive: true})
    .then(x => new Promise(resolve => setTimeout(() => resolve(x), 5000)))
    .then((response) => response.text())
    .then((response) => response.slice(response.search('id="nameGen"') + i, response.search('id="nameGen"') + i + 1000))
    .then((text) => console.log(text));

async function asyncCall() {
    let response = await fetch('https://fr.fantasynamegenerators.com/dnd-noms-elfes.php');
    let data = await response.text();
    let test = await data.slice(data.search('id="nameGen"') + i, data.search('id="nameGen"') + i + 1000);

    return test;
};

asyncCall()
    .then(response => console.info(response))